import type { FunctionComponent } from 'react'
import type { Unit } from 'convert-units'
import convert from 'convert-units'
import React from 'react'

export const contextDefaults = {
    editedId: '',
    editedDate: Date.now(),
    editedValue: convert(50).from('kg').to('mcg'),
    /**
     * If true, user is editing a measurement. This is true for
     * both creating and editing measurements.
     */
    isEditingMeasurement: false,
    /**
     * If true, user is editing an already existing measurement.
     * This is true only if user selects a preexisting measurement
     * to edit.
     */
    isEditingExistingPoint: false,
    /**
     * All data is received in micrograms, to allow potential unit conversion
     * at any time.
     */
    viewWeightUnit: 'kg' as Unit,
}

export type State = typeof contextDefaults
export type Actions =
    | {
          type: 'setEditedId'
          newId: string
      }
    | {
          type: 'setEditedDate'
          newDate: number
      }
    | {
          type: 'setEditedValue'
          newValue: number
      }
    | {
          type: 'setIsEditingData'
          isEditing: boolean
      }
    | {
          type: 'setIsEditingExistingPoint'
          isEditing: boolean
      }
export type ChartDispatch = React.Dispatch<Actions>

export const ChartContext = React.createContext(contextDefaults)
export const ChartContextDispatch = React.createContext<ChartDispatch>({} as React.Dispatch<Actions>)

// @ts-ignore
const reducer = (state: State, action: Actions): State => {
    switch (action.type) {
        case 'setEditedId':
            return { ...state, editedId: action.newId }
        case 'setEditedDate':
            return { ...state, editedDate: action.newDate }
        case 'setEditedValue':
            return { ...state, editedValue: action.newValue }
        case 'setIsEditingData':
            if (action.isEditing) {
                return { ...state, isEditingMeasurement: action.isEditing }
            } else {
                return { ...contextDefaults, isEditingMeasurement: action.isEditing }
            }
        case 'setIsEditingExistingPoint':
            return { ...state, isEditingExistingPoint: action.isEditing }
        default:
            break
    }
}

export const ProvideChartContext: FunctionComponent = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, contextDefaults)

    return (
        <ChartContextDispatch.Provider value={dispatch}>
            <ChartContext.Provider value={state}>{children}</ChartContext.Provider>
        </ChartContextDispatch.Provider>
    )
}

export const useChartContext = () => React.useContext(ChartContext)
export const useChartContextDispatch = () => React.useContext(ChartContextDispatch)
