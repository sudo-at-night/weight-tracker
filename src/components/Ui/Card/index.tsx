import type { FunctionComponent } from 'react'
import classnames from 'classnames'
import classes from './Card.module.scss'

type TCardProps = {
    className?: string
}

export const Card: FunctionComponent<TCardProps> = ({ children, className }) => {
    return <div className={classnames(classes.container, className)}>{children}</div>
}
