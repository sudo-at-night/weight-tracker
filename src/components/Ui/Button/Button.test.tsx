import { fireEvent, screen, render } from '@testing-library/react'
import { Button } from './index'

const onClick = jest.fn()

test('Button renders and does not crash', () => {
    const button = render(<Button onClick={onClick}>Hello</Button>)

    expect(button).toBeTruthy()
})

test('onClick is called correctly after clicking the button', () => {
    render(<Button onClick={onClick}>Hello</Button>)

    fireEvent.click(screen.getByTestId('button'))

    expect(onClick).toHaveBeenCalled()
})

test('onClick is not called if button is disabled and after clicking the button', () => {
    render(
        <Button onClick={onClick} disabled={true}>
            Hello
        </Button>
    )

    fireEvent.click(screen.getByTestId('button'))

    expect(onClick).toHaveBeenCalledTimes(0)
})
