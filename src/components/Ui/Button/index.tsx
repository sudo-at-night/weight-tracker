import type { FunctionComponent, MouseEventHandler } from 'react'
import classnames from 'classnames'
import classes from './Button.module.scss'

type TButtonProps = {
    className?: string
    disabled?: boolean
    theme?: 'green' | 'red'
    onClick: MouseEventHandler<HTMLButtonElement>
}

const themeMap = {
    'green': classes.greenButton,
    'red': classes.redButton,
    'disabled-green': classes.greenDisabled,
    'disabled-red': classes.redDisabled,
} as {
    [key: string]: string
}

export const Button: FunctionComponent<TButtonProps> = ({
    children,
    disabled = false,
    className,
    onClick,
    theme = 'green',
}) => {
    const themedClass = disabled ? themeMap[`disabled-${theme}`] : themeMap[theme]

    return (
        <button
            className={classnames(classes.button, themedClass, className)}
            disabled={disabled}
            type="button"
            onClick={onClick}
            data-testid="button"
        >
            {children}
        </button>
    )
}
