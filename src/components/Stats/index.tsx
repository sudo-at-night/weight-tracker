import type { FunctionComponent } from 'react'
import classnames from 'classnames'
import { Button } from 'components/Ui/Button'
import { useChartContextDispatch } from 'context/ChartContext'
import utilClasses from 'scss/modules/utils.module.scss'
import classes from './Stats.module.scss'

type TStats = {
    measurements: number
    days: number
}

type TStatsProps = TStats

export const Stats: FunctionComponent<TStatsProps> = ({ measurements, days }) => {
    const dispatch = useChartContextDispatch()

    const daysLabel = days === 1 ? 'day' : 'days'

    return (
        <>
            <p className={classes.stat}>
                <span className={classnames(utilClasses.boxy, classes.statNumber)}>{measurements}</span>{' '}
                measurements
            </p>
            <p className={classes.stat}>
                <span className={classnames(utilClasses.boxy, classes.statNumber)}>{days}</span> {daysLabel}
            </p>
            <Button
                className={classes.addButton}
                onClick={() => dispatch({ type: 'setIsEditingData', isEditing: true })}
            >
                add new measurement
            </Button>
        </>
    )
}
