import type { FunctionComponent } from 'react'
import classnames from 'classnames'
import { useUserData } from 'query/hooks'
import utilClasses from 'scss/modules/utils.module.scss'
import classes from './Header.module.scss'

export const Header: FunctionComponent = () => {
    const { data } = useUserData('test@test.com')

    const firstName = data?.info?.first_name || ''
    const lastName = data?.info?.last_name || ''

    const initials = `${firstName[0]}.${lastName[0]}.`

    return (
        <header className={classnames(utilClasses.pageContainer, classes.container)}>
            <p className={classes.avatar}>{initials}</p>
            <p className={classes.name}>
                {firstName} {lastName}
            </p>
        </header>
    )
}
