import type { ChangeEvent, FunctionComponent } from 'react'
import React from 'react'
import { format } from 'date-fns'
import convert from 'convert-units'
import { Button } from 'components/Ui/Button'
import { useChartContext, useChartContextDispatch } from 'context/ChartContext'
import { queryClient } from 'query/client'
import { useUserData, useDeleteMeasurement, useSetMeasurement } from 'query/hooks'
import { QUERIES } from 'query/queries'
import classes from './EditMeasurement.module.scss'

export const EditMeasurement: FunctionComponent = () => {
    const dispatch = useChartContextDispatch()
    const { editedDate, editedId, editedValue, isEditingExistingPoint, viewWeightUnit } = useChartContext()
    // I assume we only have one measurement (weight), so we know to convert from weight.
    // Later the user can input the weight in the view format, for instance the hardcoded KG.
    const initialValue = editedValue !== 0 ? convert(editedValue).from('mcg').to(viewWeightUnit) : 0
    const initialDate = editedDate !== 0 ? format(editedDate, 'yyyy-MM-dd') : format(Date.now(), 'yyyy-MM-dd')

    const [date, setDate] = React.useState('')
    const [value, setValue] = React.useState(0)

    const { isLoading: isUserDataLoading } = useUserData('test@test.com')
    const measurementDeleteMutation = useDeleteMeasurement('test@test.com', editedId)
    const measurementMutation = useSetMeasurement(
        'test@test.com',
        editedId,
        new Date(date).getTime(),
        editedValue
    )
    const isRequestInProgress =
        measurementMutation.isLoading || measurementDeleteMutation.isLoading || isUserDataLoading

    React.useEffect(() => {
        setDate(initialDate)
        setValue(initialValue)
    }, [editedDate, editedValue, initialDate, initialValue])

    const handleDateChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newDate = e.currentTarget.value
        dispatch({ type: 'setEditedDate', newDate: new Date(newDate).getTime() })
        setDate(newDate)
    }

    const handleValueChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newValue = parseInt(e.currentTarget.value)
        setValue(newValue)
    }

    const handleValueBlur = () => {
        // Allow 1 - 300 range
        const newValueAdjusted = Math.min(300, Math.max(1, value))
        dispatch({
            type: 'setEditedValue',
            newValue: convert(newValueAdjusted).from(viewWeightUnit).to('mcg'),
        })
        if (newValueAdjusted === 1 || newValueAdjusted === 300) {
            setValue(newValueAdjusted)
        } else if (!newValueAdjusted) {
            dispatch({
                type: 'setEditedValue',
                newValue: convert(50).from(viewWeightUnit).to('mcg'),
            })
            setValue(50)
        }
    }

    const handleCancel = () => {
        dispatch({ type: 'setIsEditingData', isEditing: false })
    }

    const handleSave = async () => {
        const mutation = await measurementMutation.mutateAsync()
        if (mutation.success) {
            queryClient.invalidateQueries([QUERIES.GET_USER_DATA])
            dispatch({ type: 'setIsEditingData', isEditing: false })
        }
    }

    const handleDelete = async () => {
        const mutation = await measurementDeleteMutation.mutateAsync()
        if (mutation.success) {
            queryClient.invalidateQueries([QUERIES.GET_USER_DATA])
            dispatch({ type: 'setIsEditingData', isEditing: false })
        }
    }

    return (
        <>
            <p className={classes.title}>Edit Measurement</p>
            <label className={classes.input}>
                Date
                <input
                    type="date"
                    disabled={isEditingExistingPoint || isRequestInProgress}
                    value={date}
                    onChange={handleDateChange}
                />
            </label>
            <label className={classes.input}>
                Weight in {viewWeightUnit}
                <input
                    type="number"
                    disabled={isRequestInProgress}
                    min={1}
                    max={300}
                    value={value}
                    onChange={handleValueChange}
                    onBlur={handleValueBlur}
                />
            </label>
            <div className={classes.buttons}>
                <Button onClick={handleCancel} disabled={isRequestInProgress}>
                    Cancel
                </Button>
                <Button onClick={handleSave} disabled={isRequestInProgress}>
                    Save
                </Button>
            </div>
            {isEditingExistingPoint && (
                <div className={classes.buttons}>
                    <Button onClick={handleDelete} disabled={isRequestInProgress} theme="red">
                        Delete
                    </Button>
                </div>
            )}
        </>
    )
}
