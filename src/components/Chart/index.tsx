import type { FunctionComponent } from 'react'
import React from 'react'
import isEqual from 'lodash/isEqual'
import debounce from 'lodash/debounce'
import convert from 'convert-units'
import { isSameDay } from 'date-fns'
import { useChartContext, useChartContextDispatch } from 'context/ChartContext'
import { drawChart } from './drawChart'
import classes from './Chart.module.scss'

export type TChartDataPoint = {
    id: string
    type: 'weight'
    value: number
    date: number
    isBeingEdited?: boolean
}

type TChartProps = {
    data: TChartDataPoint[]
}

const compareProps = (prevProps: TChartProps, nextProps: TChartProps) => {
    const { data: prevData } = prevProps
    const { data: nextData } = nextProps

    return isEqual(prevData, nextData)
}

export const Chart: FunctionComponent<TChartProps> = React.memo(({ data }) => {
    const dispatch = useChartContextDispatch()
    const [cachedData, setCachedData] = React.useState<TChartDataPoint[]>([])
    const { editedDate, editedValue, isEditingMeasurement, viewWeightUnit } = useChartContext()
    const chartEl: React.Ref<HTMLDivElement> = React.useRef(null)

    const dataWithCorrectUnits = data.map((datum) => ({
        ...datum,
        value: convert(datum.value).from('mcg').to(viewWeightUnit),
    }))
    if (isEditingMeasurement) {
        const prevMeasurement = dataWithCorrectUnits.findIndex((datum) => isSameDay(datum.date, editedDate))
        if (prevMeasurement !== -1) {
            dataWithCorrectUnits.splice(prevMeasurement, 1)
        }

        dataWithCorrectUnits.push({
            id: '',
            date: editedDate,
            value: convert(editedValue).from('mcg').to(viewWeightUnit),
            type: 'weight',
            isBeingEdited: true,
        })
    }

    const onEdit = React.useCallback(
        (d: TChartDataPoint) => {
            dispatch({ type: 'setEditedId', newId: d.id })
            dispatch({ type: 'setEditedDate', newDate: d.date })
            dispatch({ type: 'setEditedValue', newValue: convert(d.value).from(viewWeightUnit).to('mcg') })
            dispatch({ type: 'setIsEditingData', isEditing: true })
            dispatch({ type: 'setIsEditingExistingPoint', isEditing: d.id ? true : false })
        },
        [dispatch, viewWeightUnit]
    )

    React.useLayoutEffect(() => {
        const shouldRedraw = !isEqual(cachedData, data)

        if (chartEl.current && shouldRedraw) {
            let scrollEl = chartEl.current.querySelector(`.${classes.chart}`)
            const scroll = scrollEl?.scrollLeft || 0
            setCachedData(data)
            drawChart(dataWithCorrectUnits, chartEl.current, onEdit)
            scrollEl = chartEl.current.querySelector(`.${classes.chart}`)
            if (scrollEl) {
                scrollEl.scrollLeft = scroll
            }
        }
    }, [cachedData, data, dataWithCorrectUnits, onEdit])

    React.useEffect(() => {
        const resizeHandler = debounce(() => {
            if (chartEl.current) {
                drawChart(dataWithCorrectUnits, chartEl.current, onEdit)
            }
        }, 100)

        window.addEventListener('resize', resizeHandler)

        return () => {
            window.removeEventListener('resize', resizeHandler)
        }
    })

    if (!data.length) {
        return (
            <p className={classes.noDataText} data-testid="no-data-info">
                No measurements yet :(
            </p>
        )
    }

    return <div id="chart" className={classes.container} ref={chartEl} data-testid="chart"></div>
}, compareProps)
