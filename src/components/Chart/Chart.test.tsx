import type { TChartDataPoint } from './index'
import { render, screen } from '@testing-library/react'
import { Chart } from './index'

jest.mock('./drawChart', () => ({
    drawChart: jest.fn(),
}))

const mockChartData: TChartDataPoint[] = [
    {
        id: 'one',
        date: 100,
        value: 80,
        type: 'weight',
    },
    {
        id: 'one',
        date: 100,
        value: 80,
        type: 'weight',
    },
]

test('Chart renders and does not crash', () => {
    const app = render(<Chart data={[]} />)

    expect(app).toBeTruthy()
})

test('Chart renders "no data" info if passed empty array', () => {
    render(<Chart data={[]} />)

    expect(screen.getByTestId('no-data-info')).toHaveTextContent('No measurements')
})

test('Chart renders a chart element if passed a non empty array', () => {
    render(<Chart data={mockChartData} />)

    expect(screen.getByTestId('chart')).toBeTruthy()
})
