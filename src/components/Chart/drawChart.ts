import type { TChartDataPoint } from './index'
import { hoursToMilliseconds, differenceInCalendarDays } from 'date-fns'
import * as d3 from 'd3'
import classes from './Chart.module.scss'

export const drawChart = (data: TChartDataPoint[], chart: HTMLDivElement, onEdit: Function) => {
    d3.select(chart).selectAll('*').remove()
    if (!data.length) return

    const editMeasurement = (d: TChartDataPoint) => onEdit(d)

    const chartBox = chart.getBoundingClientRect()
    const sortedData = data.slice().sort((a, b) => d3.ascending(a.date, b.date))

    const oneDayWidth = 70
    const firstLastDayDiff = differenceInCalendarDays(
        sortedData[sortedData.length - 1].date,
        sortedData[0].date
    )
    const margin = { top: 10, right: 40, bottom: 30, left: 30 }
    const width = chartBox.width - margin.left - margin.right
    const height = 250 - margin.top - margin.bottom
    const overwidth = firstLastDayDiff * oneDayWidth
    const chartWidth = overwidth > width ? overwidth : width

    const parent = d3
        .select(chart)
        .append('div')
        .attr('class', classes.chart)
        .style('width', `${width + margin.left + margin.right}px`)
        .style('overflow-x', 'scroll')
        .style('overflow-y', 'hidden')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height)

    const yOverlay = parent
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .style('position', 'absolute')
        .style('z-index', 1)
        .append('g')
        .style('fill', 'red')
        .attr('transform', `translate(${margin.left}, ${margin.top})`)

    const xBody = parent
        .append('svg')
        .attr('width', chartWidth + margin.right + margin.left)
        .attr('height', height + margin.top + margin.bottom)
        .style('position', 'relative')
        .style('z-index', 5)
        .append('g')
        .attr('transform', `translate(${margin.left}, ${margin.top})`)

    const xExtent = d3.extent(sortedData, (d) => d.date)
    const x = d3
        .scaleUtc()
        // @ts-ignore
        .domain([xExtent[0] - hoursToMilliseconds(48), xExtent[1] + hoursToMilliseconds(48)])
        .range([0, chartWidth])
    xBody
        .append('g')
        .attr('class', 'xAxis')
        .attr('transform', `translate(0, ${height})`)
        .call(d3.axisBottom(x).ticks(d3.utcDay.every(1)).tickSizeOuter(0))

    const y = d3
        .scaleLinear()
        // @ts-ignore
        .domain([0, d3.max(sortedData, (d) => d.value) * 1.5])
        .nice(10)
        .range([height, margin.top])
    yOverlay.append('g').attr('class', 'yAxis').call(d3.axisLeft(y).ticks(4))

    const line = d3
        .line()
        .curve(d3.curveLinear)
        // @ts-ignore
        .x((d) => x(d.date))
        // @ts-ignore
        .y((d) => y(d.value))
    xBody
        .append('path')
        .datum(sortedData)
        .attr('class', classes.chartLine)
        .attr('fill', 'none')
        // @ts-ignore
        .attr('d', line)

    // Label point
    xBody
        .append('g')
        .selectAll('circle')
        .data(sortedData)
        .join('circle')
        .attr('class', classes.chartCircle)
        .attr('cx', (d) => x(d.date))
        .attr('cy', (d) => y(d.value))
        .attr('r', 6)
        .attr('role', 'button')
        .attr('tabindex', 0)
        .style('fill', (d) => (d.isBeingEdited ? '#a6d8bb' : null))
        .on('click', (e, d) => editMeasurement(d))
        .on('keypress', (e, d) => {
            if (e.keyCode === 32 || e.keyCode === 13) {
                editMeasurement(d)
            }
        })

    // Label
    xBody
        .append('g')
        .attr('class', classes.chartLabel)
        .selectAll('g')
        .data(sortedData)
        .join('g')
        // @ts-ignore
        .attr('transform', (d) => `translate(${x(d.date)}, ${y(d.value) - 10})`)
        .append('text')
        // @ts-ignore
        .text((d) => `${d.value} KG`)
        .attr('text-anchor', 'middle')
        .attr('fill', (d) => (d.isBeingEdited ? '#a6d8bb' : null))
}
