import type { FunctionComponent } from 'react'
import { useIsMutating } from 'react-query'
import classnames from 'classnames'
import { Chart } from 'components/Chart'
import { EditMeasurement } from 'components/EditMeasurement'
import { Stats } from 'components/Stats'
import { Card } from 'components/Ui/Card'
import { useChartContext } from 'context/ChartContext'
import { useUserData } from 'query/hooks'
import { QUERIES } from 'query/queries'
import utilClasses from 'scss/modules/utils.module.scss'
import classes from './TrackerChart.module.scss'

export const TrackerChart: FunctionComponent = () => {
    const { isFetching: isUserDataFetching, views } = useUserData('test@test.com')
    const { isEditingMeasurement } = useChartContext()
    const updateRequests = useIsMutating([QUERIES.UPDATE_USER_DATA])
    const deleteRequests = useIsMutating([QUERIES.DELETE_USER_DATA])
    const isChartRequestInProgress = updateRequests || deleteRequests || isUserDataFetching

    const chartData = views.chartDataView
    const statsData = views.statsDataView

    const CardContent = isEditingMeasurement ? (
        <EditMeasurement />
    ) : (
        <Stats measurements={statsData.measurements ?? 0} days={statsData.days ?? 0} />
    )

    return (
        <div className={classnames(utilClasses.pageContainer, classes.container)}>
            <Card className={classes.infoColumn}>{CardContent}</Card>
            <div
                className={classnames(
                    classes.chartColumn,
                    isChartRequestInProgress ? classes.disabledColumn : null
                )}
            >
                <Chart data={chartData} />
            </div>
        </div>
    )
}
