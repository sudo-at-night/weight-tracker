import { useMutation, useQuery } from 'react-query'
import * as api from 'api/calls'
import { QUERIES } from './queries'
import { chartDataView, statsDataView } from './views'

export const useUserData = (email: string, queryOptions?: any) => {
    const query = useQuery<api.TCallUserDataResponse, Error>(
        [QUERIES.GET_USER_DATA, email],
        () => api.callLogIn({ email }),
        {
            staleTime: 10000,
            refetchOnWindowFocus: false,
            ...queryOptions,
        }
    )

    const views = {
        chartDataView: chartDataView(query.data),
        statsDataView: statsDataView(query.data),
    }

    return { ...query, views }
}

export const useSetMeasurement = (
    email: string,
    id: string,
    date: number,
    value: number,
    queryOptions?: any
) => {
    const mutation = useMutation<api.TCallSetMeasurementResponse, Error>(
        [QUERIES.UPDATE_USER_DATA, email],
        () => api.callSetMeasurement({ email, id, date, value })
    )

    return mutation
}

export const useDeleteMeasurement = (email: string, id: string, queryOptions?: any) => {
    const mutation = useMutation<api.TCallDeleteMeasurementResponse, Error>(
        [QUERIES.DELETE_USER_DATA, email],
        () => api.callDeleteMeasurement({ email, id })
    )

    return mutation
}
