import { addDays } from 'date-fns'
import { chartDataView, statsDataView } from './views'

const startDate = new Date(1)

test('chartDataView() -> returns measurements from API response', () => {
    const mockResponse = {
        measurements: [{ date: startDate }],
    }

    const result = chartDataView(mockResponse as any)

    expect(result).toEqual([{ date: startDate }])
})

test.each`
    firstMeasurement                              | secondMeasurement                              | expectedMeasurements | expectedDays
    ${{ date: startDate.getTime() }}              | ${{ date: addDays(startDate, 10).getTime() }}  | ${2}                 | ${10}
    ${{ date: addDays(startDate, 10).getTime() }} | ${{ date: startDate.getTime() }}               | ${2}                 | ${10}
    ${{ date: addDays(startDate, 30).getTime() }} | ${{ date: addDays(startDate, 135).getTime() }} | ${2}                 | ${105}
`(
    'statsDataView() -> return proper stats for first measurement with date $firstMeasurement and second measurement date $secondMeasurement',
    ({ firstMeasurement, secondMeasurement, expectedMeasurements, expectedDays }) => {
        const mockResponse = {
            measurements: [firstMeasurement, secondMeasurement],
        }

        const result = statsDataView(mockResponse as any)

        expect(result.measurements).toEqual(expectedMeasurements)
        expect(result.days).toEqual(expectedDays)
    }
)

test('statsDataView() -> returns proper stats if no measurements are given', () => {
    const mockResponse = {
        measurements: [],
    }

    const result = statsDataView(mockResponse as any)

    expect(result.measurements).toEqual(0)
    expect(result.days).toEqual(0)
})

test('statsDataView() -> returns proper stats if only one measurement is given', () => {
    const mockResponse = {
        measurements: [{ date: startDate }],
    }

    const result = statsDataView(mockResponse as any)

    expect(result.measurements).toEqual(1)
    expect(result.days).toEqual(1)
})
