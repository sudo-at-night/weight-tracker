import type { TCallUserDataResponse } from 'api/calls'
import type { TChartDataPoint } from 'components/Chart'
import { differenceInCalendarDays } from 'date-fns'
import * as d3 from 'd3'

/**
 * Prepare user data for chart component
 */
export const chartDataView = (raw?: TCallUserDataResponse): TChartDataPoint[] => {
    if (!raw) return []

    return raw.measurements
}

/**
 * Prepare user data for being displayed as statistics
 */
export const statsDataView = (raw?: TCallUserDataResponse) => {
    if (!raw) return {}

    const minMaxMeasurements = d3.extent(raw.measurements, (m) => m.date)
    const measurements = raw.measurements.length
    const days =
        minMaxMeasurements[0] && minMaxMeasurements[1]
            ? differenceInCalendarDays(new Date(minMaxMeasurements[1]), new Date(minMaxMeasurements[0]))
            : 0

    return {
        measurements,
        days: measurements === 1 ? 1 : days || 0,
    }
}
