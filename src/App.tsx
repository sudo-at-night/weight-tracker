import type { FunctionComponent } from 'react'
import React from 'react'
import classnames from 'classnames'
import { Header } from 'components/Header'
import { ProvideChartContext } from 'context/ChartContext'
import { TrackerChart } from 'views/TrackerChart'
import { useUserData } from 'query/hooks'
import classes from './App.module.scss'

const App: FunctionComponent = () => {
    const [isReady, setIsReady] = React.useState(false)
    const [shouldDisplayApp, setShouldDisplayApp] = React.useState(false)
    const { isLoading } = useUserData('test@test.com')

    React.useEffect(() => {
        if (!isLoading) {
            setIsReady(true)
            setTimeout(() => {
                setShouldDisplayApp(true)
            }, 1000)
        }
    }, [isLoading])

    if (!shouldDisplayApp) {
        return <Loader fade={isReady} />
    }

    return (
        <div className={classes.container}>
            <Header />
            <ProvideChartContext>
                <TrackerChart />
            </ProvideChartContext>
        </div>
    )
}

type TLoaderProps = {
    fade: boolean
}

const Loader: FunctionComponent<TLoaderProps> = ({ fade }) => {
    return <p className={classnames(classes.loader, fade ? classes.loaderFadeOut : null)}>(WEIGHT) TRACKER</p>
}

export default App
