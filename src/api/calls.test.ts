jest.mock('api/client', () => ({
    client: {
        call: jest.fn(),
    },
}))

import { client } from 'api/client'
import * as CALLS from './calls'
import * as ENDPOINTS from './endpoints'

const call = <jest.MockedFunction<typeof client.call>>client.call

test('User Data -> Uses email to call the API with given email as URL parameter', () => {
    const credentials = { email: 'test@test.com' }

    CALLS.callLogIn(credentials)

    const callEndpoint = call.mock.calls[0][0]

    expect(callEndpoint).toEqual(`${ENDPOINTS.USER_DATA}/test@test.com`)
})

test('Set Measurement -> Uses email, measurement id, date and measurement value to call the API with given email as URL parameter and rest as body', () => {
    const args = { email: 'test@test.com', id: '123098', date: 15, value: 40 }

    CALLS.callSetMeasurement(args)

    const callEndpoint = call.mock.calls[0][0]
    const callBody = call.mock.calls[0][1].body

    expect(callEndpoint).toEqual(`${ENDPOINTS.USER_DATA}/test@test.com/measurement`)
    expect(callBody).toEqual(
        JSON.stringify({ email: args.email, id: args.id, date: args.date, value: args.value })
    )
})

test('Delete Measurement -> Uses email and measurement id to call the API with given email as URL parameter and rest as body', () => {
    const args = { email: 'test@test.com', id: '123098' }

    CALLS.callDeleteMeasurement(args)

    const callEndpoint = call.mock.calls[0][0]
    const callBody = call.mock.calls[0][1].body

    expect(callEndpoint).toEqual(`${ENDPOINTS.USER_DATA}/test@test.com/measurement`)
    expect(callBody).toEqual(JSON.stringify({ email: args.email, id: args.id }))
})
