import { client } from 'api/client'
import * as ENDPOINTS from 'api/endpoints'

export type TCallUserDataArgs = {
    email: string
}

export type TCallUserDataResponse = {
    email: string
    info: {
        first_name: string
        last_name: string
    }
    measurements: {
        id: string
        type: 'weight'
        value: number
        date: number
    }[]
}

/**
 * Get user's data together with his measurements
 */
export async function callLogIn(args: TCallUserDataArgs): Promise<TCallUserDataResponse> {
    const { email } = args

    const response = await client.call(`${ENDPOINTS.USER_DATA}/${email}`, {
        method: 'GET',
    })

    if (!response.ok) {
        throw new Error('Could not fetch user data')
    }

    return await response.json()
}

export type TCallSetMeasurementArgs = {
    email: string
    /**
     * ID of a measurement
     */
    id: string
    date: number
    value: number
}

export type TCallSetMeasurementResponse = {
    success: boolean
}

/**
 * Set user's measurement data
 */
export async function callSetMeasurement(
    args: TCallSetMeasurementArgs
): Promise<TCallSetMeasurementResponse> {
    const { email, id, date, value } = args

    const response = await client.call(`${ENDPOINTS.USER_DATA}/${email}/measurement`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, id, date, value }),
    })

    if (!response.ok) {
        throw new Error('Could not save measurement data')
    }

    return await response.json()
}

export type TCallDeleteMeasurementArgs = {
    email: string
    /**
     * ID of a measurement
     */
    id: string
}

export type TCallDeleteMeasurementResponse = {
    success: boolean
}

/**
 * Delete user's measurement data
 */
export async function callDeleteMeasurement(
    args: TCallDeleteMeasurementArgs
): Promise<TCallDeleteMeasurementResponse> {
    const { email, id } = args

    const response = await client.call(`${ENDPOINTS.USER_DATA}/${email}/measurement`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, id }),
    })

    if (!response.ok) {
        throw new Error('Could not delete measurement data')
    }

    return await response.json()
}
