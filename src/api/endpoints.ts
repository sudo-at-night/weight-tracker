/**
 * Used for grabbing user data.
 */
export const USER_DATA = '/user'
