import type { Server } from 'miragejs'
import { Response } from 'miragejs'
import { apiURL } from 'api/client'
import { RandomHash } from 'random-hash'
import * as ENDPOINTS from 'api/endpoints'

const random = new RandomHash({ length: 20 })

export function routes(this: Server) {
    this.urlPrefix = apiURL || ''
    this.timing = 1000

    this.get(`${ENDPOINTS.USER_DATA}/:email`, (schema: any, request) => {
        const email = request.params.email
        const user = schema.users.findBy({ email })

        if (!user) {
            return new Response(404)
        }

        return new Response(200, {}, user.attrs)
    })

    this.post(`${ENDPOINTS.USER_DATA}/:email/measurement`, (schema: any, request) => {
        const email = request.params.email
        const { id, date, value } = JSON.parse(request.requestBody)
        const user = schema.users.findBy({ email })

        if (!user) {
            return new Response(404)
        }

        const measurements = [...user.attrs.measurements]
        const existingMeasurement =
            measurements.find((m: any) => m.date === date) || measurements.find((m: any) => m.id === id)

        if (existingMeasurement) {
            const existingIndex = measurements.findIndex((m) => m.id === existingMeasurement.id)
            measurements.splice(existingIndex, 1)
        }

        const newMeasurement = {
            id: id || random(),
            date,
            value,
            type: 'weight', // Frontend could provide us this info but let's say we always mean this on both ends
        }
        measurements.push(newMeasurement)

        user.update({ measurements })

        return new Response(200, {}, { success: true })
    })

    this.delete(`${ENDPOINTS.USER_DATA}/:email/measurement`, (schema: any, request) => {
        const email = request.params.email
        const { id } = JSON.parse(request.requestBody)
        const user = schema.users.findBy({ email })

        if (!user) {
            return new Response(404)
        }

        const measurements = [...user.attrs.measurements]
        const existingMeasurement = measurements.find((m: any) => m.id === id)

        if (existingMeasurement) {
            const existingIndex = measurements.findIndex((m) => m.id === existingMeasurement.id)
            measurements.splice(existingIndex, 1)
        } else {
            return new Response(400)
        }

        user.update({ measurements })

        return new Response(200, {}, { success: true })
    })
}
