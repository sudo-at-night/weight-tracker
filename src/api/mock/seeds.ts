import type { Server } from 'miragejs'

export function seeds(server: Server) {
    server.create('user', {
        email: 'test@test.com',
        info: {
            first_name: 'Christopher',
            last_name: 'Anonymous',
        },
        measurements: [],
    } as any)
}
