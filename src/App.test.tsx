import { render } from '@testing-library/react'
import App from './App'

test('App renders and does not crash', () => {
    const app = render(<App />)

    expect(app).toBeTruthy()
})
