# The (Weight) Tracker

## How to run

### Requirements

The Node.js part of the following stack can be installed locally via [NVM](https://github.com/nvm-sh/nvm) since it can speed up version selection and Yarn installation.

-   [Node.js](https://nodejs.org/en) >= 14.x.x
-   [Yarn](https://yarnpkg.com) >= 1.22.x

### Commands

This project uses Yarn as the package manager.

```sh
# Create local .env file
cp .env.example .env

# If Yarn is not installed and NVM was used, it can be installed via NPM
npm i -g yarn

# Install project and dependencies
yarn

# Start project locally
yarn start

# Start project locally and open the browser automatically
yarn start:preview

# Run tests
yarn test

# Format the code
yarn format
```

### Local test environment

The project is using MirageJS to mock some server responses. The option to turn it off can be found under `.env.example`.

# Notes

## API user data

There is only one user, to focus on creating the frontend I used a simple GET request to grab user data. Normally this would be placed behind an authentication and authorization mechanism.

## `node-sass` and new `math.div`

I'd normally use a function to recalculate pixels into REM units. I also did it here but due to recent change in SASS/SCSS a simple `/` is now deprecated, due to how it's also used in CSS by itself. `math.div` is the new replacement, however if trying to use it with `node-sass` it'll just break. I left it as is to focus on the rest of the app.

## Animations and transitions

To make the code easier to ready I've tried to minimize the amount of animation-related code to minimum. Due to this I relied on the `setTimeout` to create the initial fade-in anmiation once user data is loaded up.

## No code-splitting, no suspense

Since this is a simple app, there was no need to split components into being loaded lazily. `React.lazy()`, together with `React.Suspense()` could have been used together to specify boundaries for loading states, while separate components were loaded into the browser on demand.

## No routing

Again, a simple app like this requires no routing system. If it was required, adding it would be very easy via `react-router`. This would allow for specifying what should be rendered under which path/hash path.
