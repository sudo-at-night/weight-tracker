import { QUERIES } from '../../src/query/queries'

const mockResponses = {
    [`${QUERIES.GET_USER_DATA}-test@test.com`]: {
        email: 'test@test.com',
        info: {
            first_name: 'Patrick',
            last_name: 'The Fish',
        },
        measurements: [
            {
                id: '1',
                type: 'weight',
                value: 15,
                date: 1,
            },
        ],
    },
}

export const useQuery = ([queryKey, email]) => {
    return {
        data: mockResponses[`${queryKey}-${email}`],
    }
}

export const useMutation = () => {}

export class QueryClient {}
